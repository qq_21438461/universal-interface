
#include "ThreadPool.h"

ThreadPool::ThreadPool(size_t maxThreads) : m_stopflag(false), m_maxThreads(maxThreads), m_workingThreads(0), m_workers(),
                                                m_priorityTasks(), m_queue_mutex(), m_condition(),
                                                m_historyRecordEnabled(false), m_historyRecordLimit(100), m_historyRecordVectorMaxSize(m_historyRecordLimit+m_historyRecordLimit/2),m_historyRecordVector(),
                                                m_log_mutex()
{
  m_historyRecordVector.reserve(m_historyRecordVectorMaxSize);

  for (size_t i = 0; i < maxThreads; ++i) {
      m_workers.emplace_back(new ThreadWrapper);
      m_workers.back()->setmain([this] {
          for(;;) {//The thread function cannot exit, otherwise it will exit the thread.
              std::function<void()> task;
              {
                std::unique_lock<std::mutex> lock(this->m_queue_mutex);
retry:
                if(!this->m_priorityTasks.empty()) {
                    _updateExecTime(this->m_priorityTasks.top());
                    task = std::move(this->m_priorityTasks.top().task);
                    this->m_priorityTasks.pop();
                }
                else {
                    this->m_condition.wait(lock, [this]{ return this->m_stopflag.load() || !this->m_priorityTasks.empty(); });
                    if(this->m_stopflag.load() && this->m_priorityTasks.empty()) {
                        return;
                    }
                    goto retry;
                }
              }
              m_workingThreads.store(m_workingThreads.load() + 1, std::memory_order_release);
//              std::cout << "Working Threads: " << m_workingThreads.load() << std::endl;
              task();
              m_workingThreads.store(m_workingThreads.load() - 1, std::memory_order_release);
          }
      });
      m_workers.back()->start();
  }
}
ThreadPool::~ThreadPool(){
  {
      std::unique_lock<std::mutex> lock(m_queue_mutex);
      m_stopflag.store(true, std::memory_order_release);
  }
  m_condition.notify_all();
  for(auto &_worker : m_workers) {
      _worker->stop();
  }
}
