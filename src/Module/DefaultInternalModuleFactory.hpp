#pragma once 
#include "Module/InternalModuleFactory.hpp"
// #include "JSONHandler/JSONHandler.h"
// #include "RandomProvider/RandomProvider.h"
class DefaultInternalModuleFactory : public InternalModuleFactory {
  public:
      std::unique_ptr<JSONHandler> createJsonHandler() override;
      std::unique_ptr<RandomProvider> createRandomProvider() override; 

};
  