#include "DefaultInternalModuleFactory.hpp"
#include "JSONHandler/JSONHandler.h"
#include "RandomProvider/RandomProvider.h"

std::unique_ptr<JSONHandler> DefaultInternalModuleFactory::createJsonHandler() {
  return std::make_unique<JSONHandler>();
}

std::unique_ptr<RandomProvider> DefaultInternalModuleFactory::createRandomProvider(){
  return std::unique_ptr<RandomProvider>(new RandomProvider());
}
