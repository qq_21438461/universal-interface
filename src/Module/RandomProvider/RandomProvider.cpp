#include "RandomProvider.h"

#ifndef USE_CPP17_FEATURES
// 在类外部为静态数据成员提供初始化
std::mt19937 RandomProvider::_default_rng{getSeed()};
#endif

