#pragma once
#include <memory>
class RandomProvider;
class JSONHandler;
class InternalModuleFactory {
public:
    virtual ~InternalModuleFactory() = default;

    virtual std::unique_ptr<JSONHandler> createJsonHandler() = 0;
    virtual std::unique_ptr<RandomProvider> createRandomProvider() = 0;
};

