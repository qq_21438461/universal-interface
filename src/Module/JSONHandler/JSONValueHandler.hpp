#include <iostream>
#include <string>
#include <type_traits>
#include "nlohmann/json.hpp"
/**
 * @brief A template structure for handling JSON values using the nlohmann::json library.
 *
 * This structure provides methods to get and set values of various types from a JSON object.
 * It supports types such as bool, float, std::string, int, and std::nullptr_t.
 *
 * @tparam T The type of the value to be handled.
 */
template <typename T>
struct JSONValueHandler {
    /**
     * @brief Gets a value of type T from a nlohmann::json object.
     *
     * This method checks the type of the JSON value and returns it if the types match.
     * If the types do not match, it returns a default value for type T.
     *
     * @param j The nlohmann::json object from which the value is to be extracted.
     * @return The value of type T extracted from the JSON object, or a default value if the types do not match.
     * @throws std::runtime_error if the type T is not supported.
     */
    T getValue(const nlohmann::json& j) {
        if constexpr (std::is_same_v<T, bool>) {
            if (j.is_boolean()) {
                return j.get<bool>();
            }
            return false;
        } else if constexpr (std::is_same_v<T, float>) {
            if (j.is_number_float()) {
                return j.get<float>();
            }
            return 0.0;
        } else if constexpr (std::is_same_v<T, std::string>) {
            if (j.is_string()) {
                return j.get<std::string>();
            }
            return "";
        } else if constexpr (std::is_same_v<T, int>) {
            if (j.is_number_integer()) {
                return j.get<int>();
            }
            return 0;
        }else if constexpr (std::is_same_v<T, std::nullptr_t>) {
            if (j.is_null()) {
                return nullptr;
            }
            return nullptr;
        }
        else {
            throw std::runtime_error("Type not supported");
            return T{};
        }
    }
    /**
     * @brief Sets a value of type T in a nlohmann::json object.
     *
     * @param j The nlohmann::json object where the value is to be set.
     * @param value The value of type T to be set in the JSON object.
     */
    void setValue(nlohmann::json& j, const T& value) {
        j = value;
    }
};


