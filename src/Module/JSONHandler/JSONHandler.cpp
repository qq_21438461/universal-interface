#include "JSONHandler.h"

bool isValidFilePath(const std::string& path) {
#ifdef USE_CPP17_FEATURES 
    return std::filesystem::exists(path) && !std::filesystem::is_directory(path);
#else
    std::ifstream file(path.c_str());
    return file.good();
#endif
}

JSONHandler::JSONHandler(){
  #ifdef USE_CPP17_FEATURES 
      DefaultInternalModuleFactory _provider;
      m_randomProvider = _provider.createRandomProvider();  
  #endif      
}
//class DefaultInternalModuleFactory;
/**
 * @brief 从文件中读取json对象
 * @param path 文件路径
 * @return bool 读取成功返回true，否则返回false
 */
JSONHandler::JSONHandler(const std::string& filepath){
  loadFromJsonFile(filepath);
}
bool JSONHandler::init(const std::string* source){

  // If the source string is empty, return false immediately.
  if (source == nullptr || source->empty()) {
    std::cerr << __FUNCTION__ << " Error: empty source string." << std::endl;
    return false;
  }
  //check it is a available file path
  if (isValidFilePath(*source)) {
    if (!loadFromJsonFile(*source)) {
      std::cerr << __FUNCTION__ << " Error: failed to load JSON file: " << source << std::endl;
    }
  }
  //check it is a json string
  try {
      m_jsonObject = nlohmann::json::parse(*source);
      // Check if the JSON object is an actual object and save the first key
      if (m_jsonObject.is_object() && !m_jsonObject.empty()) {
          setMainKey(m_jsonObject.begin().key());
          m_currentKey = this->m_mainKey;
      }
      return true; // If parsing succeeds, return true immediately.
    } catch (...) {
      // Handle all exceptions here
   }

  return true;
}
/**
 * @brief 清空当前json资源之后重新加载
 * @param source JSON字符串或者JSON文件路径
 * @return bool 读取成功返回true，否则返回false
 */
bool JSONHandler::reset(const std::string* source) noexcept{
  clear();
  return init(source);
}
void JSONHandler::clear() noexcept{
#ifdef USE_CPP17_FEATURES 
  m_jsonValueMap.clear();
#endif
  m_jsonStringMap.clear();
  m_jsonObject.clear();
  m_currentKey.clear();
  m_mainKey.clear();    
}
bool JSONHandler::loadFromJsonFile(const std::string& inputFileName){
  try {
    std::ifstream jsonFile(inputFileName);
    if (!jsonFile) {
      std::cerr << "open file error" << std::endl;
      return false;
    }
    m_currentfile = inputFileName;
    jsonFile >> m_jsonObject;

    // Check if the JSON object is an actual object and save the first key
    if (m_jsonObject.is_object() && !m_jsonObject.empty()) {
        setMainKey(m_jsonObject.begin().key());
        m_currentKey = this->m_mainKey;
    }
  } catch (const nlohmann::json::parse_error& e) {
      std::cerr << __FUNCTION__ << "JSON parse error: " << e.what() << std::endl;
      return false;
  } catch (const std::exception& e) {
    std::cerr << __FUNCTION__ << "Error reading file " << inputFileName << ": " << e.what() << std::endl;
    return false;
  } catch (...) {
    std::cerr << __FUNCTION__ << "Unknown exception while reading file " << inputFileName << std::endl;
    return false;
  }
  return true;
}
bool JSONHandler::containsKey(const std::string& key) {            
  return findKeyHelper(key, m_jsonObject, false);
}
std::string JSONHandler::getValue(const std::string& key){
  if(m_jsonStringMap.empty()){
    getAllValue();
  }
  auto it = m_jsonStringMap.find(key);
  if (it != m_jsonStringMap.end()) {
      return it->second;  // 返回找到的值
  } else {
      // 返回一个默认值或抛出一个异常,这里返回一个空字符串  
      return "";  
  }
}
/**
 * @brief 保存json对象到文件
 * @param path 文件路径
 * @return bool 保存成功返回true，否则返回false
 */
bool JSONHandler::saveToFile(const std::string& outputFileName, uint8_t dumpValue,const nlohmann::json &outputjson){
  try {
      std::ofstream outputFile(outputFileName);
      //check error
      if (!outputFile) {
          std::cerr << "Failed to open file: " << outputFileName << std::endl;
          return false;
      }
      else if(outputjson.is_null()){
        std::cerr << "json object is null" << std::endl;
        return false;
      }
      //main operation
      if (!(outputFile << outputjson.dump(dumpValue))) {
          std::cerr << "Failed to write JSON data to file." << std::endl;
          return false;
      }
  } catch (const nlohmann::json::exception& e) {
      std::cerr << __FUNCTION__ << " JSON exception caught: " << e.what() << std::endl;
      return false;
  } catch (const std::exception& e) {
      std::cerr << __FUNCTION__ << " Exception caught: " << e.what() << std::endl;
      return false;
  } catch (...) {
      std::cerr << __FUNCTION__ << " Unknown exception caught." << std::endl;
      return false;
  }
  return true;
}
bool JSONHandler::getAllValue(const std::string* key, std::map<std::string, std::string>* resultMap, bool topLevelOnly) noexcept {
        std::map<std::string, std::string>* targetMap = &m_jsonStringMap;
      if (key != nullptr) {
        if (!resultMap) {
            std::cerr << "Error: When providing a key, a resultMap must also be provided." << std::endl;
            return false;
        }
        targetMap = resultMap;
      } else {
          m_jsonStringMap.clear();
      }
      exploreJsonForKey(m_jsonObject, [&](const std::string& k, const nlohmann::json& v) {
          if (!v.is_object() && !v.is_array()) {
              targetMap->emplace(k, v.dump());
              std::cout << "key: " << k << " value: " << v.dump() << std::endl;
          }
      }, topLevelOnly, key ? key : nullptr);

      return !targetMap->empty();
}
bool JSONHandler::createConfigJsonFile(const std::string& path,std::map<std::string, std::string> *configMap){
  // 创建一个新的JSON对象
  nlohmann::json configJson;
  if(configMap == nullptr){
    configJson["mainkey"] = {
        {"logConfiguration", {
            {"logFile", "/path/to/logfile"}, 
            {"logLevel", "INFO"},
            {"maxLogSize", 1048576},  // 例如，1MB
            {"logRotationCount", 5}
        }},
        {"moduleEnable", {
            {"module1", true},
            {"module2", false},
            {"module3", true}
        }},
        {"selfCheck", {
            {"cycleTime", 300},  // 例如，每300秒进行一次自检
            {"timeout", 30}  // 自检超时时间
        }},
        {"otherSettings", {
            {"setting1", "value1"},
            {"setting2", "value2"}
        }}
    };
  }else{
    configJson["mainkey"] = nlohmann::json::object();
#ifdef USE_CPP17_FEATURES     
    for(auto& [key, value] : *configMap){
      configJson["mainkey"][key] = value;
    } 
#else
    for(auto& it : *configMap){
      configJson["mainkey"][it.first] = it.second;
    }
#endif
  }
  return saveToFile(path, 4, configJson);
}


#ifdef USE_CPP17_FEATURES 
void JSONHandler::createRNGJsonFile(const std::string& path,std::size_t Max_Size_byte,int MaxDepth){
  // init a main key for json
  nlohmann::json outputJson;
  outputJson["RandomMainKey"] = nlohmann::json::object();

  // 获取"RandomMainKey"的值的指针，以便后续在这个值（一个JSON对象）中添加数据
  nlohmann::json* currentLevel = &outputJson["RandomMainKey"];
  // std::string currentKey = m_randomProvider->getRandomValue<std::string>({5, 15});
  // (*currentLevel)[currentKey] = nlohmann::json::object();
  std::size_t AllJsonSize = 1;
  int AllJsonDepth = 0;
  int currentDepth = 0 ;
  // 生成随机的JSON数据，直到JSON文件大小超过指定的最大值
  while (outputJson.dump().size() < Max_Size_byte) {
      //get depth

      if(AllJsonDepth>= MaxDepth){
        currentDepth = m_randomProvider->getRandomValue<int>({0, MaxDepth});
      }
      else{
         currentDepth = m_randomProvider->getRandomValue<int>({0, (1 + AllJsonDepth)});
        if(currentDepth > AllJsonDepth){
          AllJsonDepth = currentDepth;
        }
      }
      auto newTargetParentObject = getTargetLevel(currentLevel, currentDepth);
    //  setRandomObject(newTargetParentObject.value().get());
      std::optional<std::pair<std::string, std::string>> myOptional;
      setRandomObject(newTargetParentObject.value().get(), &myOptional); 
     std::cout << "[" << AllJsonSize << "]Key: " << myOptional->first << ", Value: " << myOptional->second << std::endl;
      ++AllJsonSize;
    }
  // 保存文件
  saveToFile(path, 4, outputJson);  
}

std::optional<std::reference_wrapper<nlohmann::json>> JSONHandler::getRandomObject(nlohmann::json& parentObject)const{
  if(parentObject.is_object() == false){
    return std::nullopt;
  }
  std::vector<std::string> objectkeys;
  for (auto& [key, value] : parentObject.items()) {
      if (value.is_object()) {
          objectkeys.push_back(key);
      }
  }
  int resultobject = m_randomProvider->getRandomValue<int>({-1, static_cast<int>(objectkeys.size()-1)});
  if(resultobject == -1){
    return std::nullopt;
  }
  return std::reference_wrapper<nlohmann::json>(parentObject[objectkeys[resultobject]]);
}
std::optional<std::reference_wrapper<nlohmann::json>> JSONHandler::getTargetLevel(nlohmann::json* rootKey, int targetDepth)const{
  if(rootKey == nullptr || targetDepth < 0 || rootKey->is_null()){
    return std::nullopt;
  }
  nlohmann::json* currentLevel = rootKey;
  
  for(int depth  = 0; depth  < targetDepth - 1; ++depth){
    auto result = getRandomObject(*currentLevel);
    if(result.has_value()){
      currentLevel = &result.value().get();
    }
    else{
      std::string newkey = m_randomProvider->getRandomValue<std::string>({5, 15});
      (*currentLevel)[newkey] = nlohmann::json::object();
      currentLevel = &(*currentLevel)[newkey];
    }
  }
  return std::reference_wrapper<nlohmann::json>(*currentLevel);
}
#endif