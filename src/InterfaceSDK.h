#pragma once 

#include <string>
#include <vector>
#include <map>
#include <memory>
#include "SoftWareVersion.h"
//class Exceptionhandler;
/*
  Because template programming encapsulation increases complexity, only non-template function API is encapsulated in impl mode.
      /\_/\  
     ( o.o ) >  
      > ^ <  
*/

class JSONHandler;
class InterfaceSDK
{

public:
  InterfaceSDK();
  ~InterfaceSDK() = default;

  bool JSONHandler_loadFromJsonFile(const std::string& path);
  std::string JSONHandler_getJsonttoStdstring();
  bool JSONHandler_containsKey(const std::string& key);
  std::string JSONHandler_getValue(const std::string& key);
  bool JSONHandler_createConfigJsonFile(const std::string& path,std::map<std::string, std::string> *configMap = nullptr); 
  bool JSONHandler_getAllValue(const std::string* key = nullptr, std::map<std::string, std::string>* resultMap = nullptr, bool topLevelOnly = false) noexcept;
  bool JSONHandler_getAllValue(const std::string& key, std::map<std::string, std::string>& resultMap, bool topLevelOnly = false) noexcept;
  bool JSONHandler_init(const std::string* source);
  bool JSONHandler_reset(const std::string* source) noexcept ;//Reset json object from file or json string
  void JSONHandler_clear() noexcept;
  bool JSONHandler_saveToFile(const std::string& outputFileName, uint8_t dumpValue = 4);
  std::string JSONHandler_getMainKey() const noexcept;
private:
  JSONHandler * m_jsonHandler;

};