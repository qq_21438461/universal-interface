#!/bin/sh

Defaultnfsaddr=192.168.31.10
Defaultmountdir=/home/nfs
Defaultmynfsdir=/nfsroot

#Determine whether the directory mounted by itself exists
if test -d $Defaultmynfsdir
then
  echo "$Defaultmynfsdir exists"
else
  mkdir $Defaultmynfsdir
fi

#Enter keywords to unmount
if [[ "$1" == "un" || "$1" == "clear" || "$1" == "no" ]]
then
   umount -v $Defaultmynfsdir  
   exit 0
fi
#Enter the specified mount point ip and directory
if [[ $# -gt 0 && -n $1 ]]
then 
  if [[ $# -gt 1 && -n $2 ]]
  then
  mount -t nfs -o nolock $1:$2 $Defaultmynfsdir
  echo "mount -t nfs -o nolock $1:$2 $Defaultmynfsdir"
  else
  mount -t nfs -o nolock $1:$Defaultmountdir /nfsroot
  echo "mount -t nfs -o nolock $1:$Defaultmountdir $Defaultmynfsdir"
  fi
 else
   echo "No parameters found, mount $Defaultnfsaddr by default"
   mount -t nfs -o nolock $Defaultnfsaddr:$Defaultmountdir $Defaultmynfsdir
fi
