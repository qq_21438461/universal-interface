#include "InterfaceSDK.h"
#include "JSONHandler/JSONHandler.h"

InterfaceSDK::InterfaceSDK(){
  m_jsonHandler = new JSONHandler();
}
bool InterfaceSDK::JSONHandler_loadFromJsonFile(const std::string& path){
  return m_jsonHandler->loadFromJsonFile(path);
}
std::string InterfaceSDK::JSONHandler_getJsonttoStdstring(){   
    return m_jsonHandler->getJsonttoStdstring();
}
bool InterfaceSDK::JSONHandler_containsKey(const std::string& key){
  return m_jsonHandler->containsKey(key);
}
std::string InterfaceSDK::JSONHandler_getValue(const std::string& key){
  return m_jsonHandler->getValue(key);
}
bool InterfaceSDK::JSONHandler_createConfigJsonFile(const std::string& path,std::map<std::string, std::string> *configMap){
  return m_jsonHandler->createConfigJsonFile(path,configMap);
}
bool InterfaceSDK::JSONHandler_getAllValue(const std::string* key, std::map<std::string, std::string>* resultMap, bool topLevelOnly) noexcept{
  return m_jsonHandler->getAllValue(key,resultMap,topLevelOnly);
}
bool InterfaceSDK::JSONHandler_getAllValue(const std::string& key, std::map<std::string, std::string>& resultMap, bool topLevelOnly) noexcept{
  return m_jsonHandler->getAllValue(key,resultMap,topLevelOnly);
}
bool InterfaceSDK::JSONHandler_init(const std::string* source){
  return m_jsonHandler->init(source);
}
bool InterfaceSDK::JSONHandler_reset(const std::string* source) noexcept{
  return m_jsonHandler->reset(source);
}
void InterfaceSDK::JSONHandler_clear() noexcept{
  m_jsonHandler->clear();
}
std::string InterfaceSDK::JSONHandler_getMainKey() const noexcept{
  return m_jsonHandler->getMainKey();
}
bool InterfaceSDK::JSONHandler_saveToFile(const std::string& outputFileName, uint8_t dumpValue){
  return m_jsonHandler->saveToFile(outputFileName,dumpValue);
}
