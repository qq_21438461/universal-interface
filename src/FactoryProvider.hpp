#pragma once
#include "DefaultInternalModuleFactory.hpp"
#include <map>
#include <functional>
/*
    At present, there is only one internal class factory. 
    This design is to be compatible with the possibility of external interface factory in the future.
*/
class FactoryProvider {
public:
    template<typename T>
    std::unique_ptr<T> createFactory(){
        if constexpr (std::is_same_v<T, DefaultInternalModuleFactory>) {
            return std::make_unique<DefaultInternalModuleFactory>();
        } else {
            throw std::runtime_error("Type not supported");
        }
    }
};
template<>
std::unique_ptr<DefaultInternalModuleFactory> FactoryProvider::createFactory(){
    return std::make_unique<DefaultInternalModuleFactory>();
}