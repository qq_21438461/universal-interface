#pragma once

#if (__cplusplus >= 201703L) 
  #if (defined(__GNUC__) && (__GNUC__ > 7)) || (defined(_MSC_VER) && (_MSC_VER >= 1910))
    #define USE_CPP17_FEATURES
  #endif
#endif