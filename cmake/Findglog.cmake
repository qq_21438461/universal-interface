cmake_minimum_required(VERSION 3.10)
# 定义一个函数 find_a
# 这个函数会查找名为 a 的库
# 如果找到了，就设置 A_FOUND 为 TRUE，同时设置 A_INCLUDE_DIRS 和 A_LIBRARIES 变量

set(CONFIGURE_DEPENDS OFF)

string(FIND ${CMAKE_CXX_COMPILER} "aarch64" compiler_var)
function(find_glog)
  if (${compiler_var} EQUAL -1)
    set(GLOG_INCLUDE_DIRS ${CMAKE_SOURCE_DIR}/third_party/ubuntu/glog/include CACHE PATH "Glog include directories" FORCE)
    find_library(GLOG_LIBRARIES NAMES libglog.a PATHS ${CMAKE_SOURCE_DIR}/third_party/ubuntu/glog/lib)
  else()
  set(GLOG_INCLUDE_DIRS ${CMAKE_SOURCE_DIR}/third_party/aarch64/glog/include CACHE  PATH "Glog include directories" FORCE)
    find_library(GLOG_LIBRARIES NAMES libglog.a PATHS ${CMAKE_SOURCE_DIR}/third_party/aarch64/glog/lib)
  endif()

#  message("find_glog:" ${GLOG_INCLUDE_DIRS}  "---" ${GLOG_LIBRARIES} "!!!!!!!!!!!")
  if(GLOG_INCLUDE_DIRS AND GLOG_LIBRARIES)
    set(GLOG_FOUND TRUE)
  endif()
endfunction()

# 调用 find_glog 函数
find_glog()

# 如果找到 a 库，则添加库的头文件目录和链接库
if(GLOG_FOUND)
#  message("GLOG_FOUND:" ${GLOG_INCLUDE_DIRS}  "-" ${GLOG_LIBRARIES})
  include_directories(${GLOG_INCLUDE_DIRS})
  target_link_libraries(${PROJECT_NAME} ${GLOG_LIBRARIES})
endif()

# 安装 glog 库的头文件和链接库到系统中，通过这个操作，其他项目或用户就可以使用这个库，而不需要再进行额外的安装或配置。
#install(DIRECTORY ${GLOG_INCLUDE_DIRS} DESTINATION include)
#install(FILES ${GLOG_LIBRARIES} DESTINATION lib)