cmake_minimum_required(VERSION 3.10)
# 定义一个函数 find_a
# 这个函数会查找名为 a 的库
# 如果找到了，就设置 A_FOUND 为 TRUE，同时设置 A_INCLUDE_DIRS 和 A_LIBRARIES 变量

message("LOG4CPLUS CMAKE_MODULE_PATH = ${CMAKE_MODULE_PATH}")

string(FIND ${CMAKE_CXX_COMPILER} "aarch64" compiler_var)
function(find_log4cplus)
  if (${compiler_var} EQUAL -1)
    set(LOG4CPLUS_INCLUDE_DIRS ${CMAKE_SOURCE_DIR}/third_party/ubuntu/log4cplus/include CACHE PATH "log4cplus include directories" FORCE)
    find_library(LOG4CPLUS_LIBRARIES NAMES liblog4cplus.a PATHS ${CMAKE_SOURCE_DIR}/third_party/ubuntu/log4cplus/lib)
  else()
    set(LOG4CPLUS_INCLUDE_DIRS ${CMAKE_SOURCE_DIR}/third_party/aarch64/log4cplus/include CACHE PATH "log4cplus include directories" FORCE)
    find_library(LOG4CPLUS_LIBRARIES NAMES liblog4cplus.a PATHS ${CMAKE_SOURCE_DIR}/third_party/aarch64/log4cplus/lib)
  endif()

  message("find_LOG4CPLUS:" ${LOG4CPLUS_INCLUDE_DIRS}  "---" ${LOG4CPLUS_LIBRARIES} "!!!!!!!!!!!")
  if(LOG4CPLUS_INCLUDE_DIRS AND LOG4CPLUS_LIBRARIES)
    set(LOG4CPLUS_FOUND TRUE)
  endif()
endfunction()

# 调用 find_glog 函数
find_log4cplus()

# 如果找到 a 库，则添加库的头文件目录和链接库
if(LOG4CPLUS_FOUND)
  message("LOG4CPLUS_FOUND:" ${GLOG_INCLUDE_DIRS}  "-" ${LOG4CPLUS_LIBRARIES})
  include_directories(${LOG4CPLUS_INCLUDE_DIRS})
  target_link_libraries(${PROJECT_NAME} ${LOG4CPLUS_LIBRARIES})
endif()

# 安装 glog 库的头文件和链接库到系统中，通过这个操作，其他项目或用户就可以使用这个库，而不需要再进行额外的安装或配置。
#install(DIRECTORY ${LOG4CPLUS_INCLUDE_DIRS} DESTINATION include)
#install(FILES ${LOG4CPLUS_LIBRARIES} DESTINATION lib)