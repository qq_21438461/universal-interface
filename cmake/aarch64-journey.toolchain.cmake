# toolchain.cmake



# Allow user to specify the cross toolchain path or use a default one
set(CROSS_TOOLCHAIN_PATH "/opt/sdk" CACHE PATH "Path to the cross compiler")

# Set the path to the cross-compilation tools
set(CROSSTOOL_PATH "${CROSS_TOOLCHAIN_PATH}")
message("sdk path: ${CROSSTOOL_PATH}")
# Set the sysroot path
set(CMAKE_SYSROOT "${CROSSTOOL_PATH}/sysroots/target-arm")

# Set the cross-compiler executables
set(CMAKE_C_COMPILER "${CROSSTOOL_PATH}/sysroots/x86_64-linux/bin/arm-openwrt-linux-muslgnueabi-gcc")
set(CMAKE_CXX_COMPILER "${CROSSTOOL_PATH}/sysroots/x86_64-linux/bin/arm-openwrt-linux-muslgnueabi-g++")

# Set other toolchain executables
set(CMAKE_ASM_COMPILER "${CROSSTOOL_PATH}/sysroots/x86_64-linux/bin/arm-openwrt-linux-muslgnueabi-as")
set(CMAKE_AR "${CROSSTOOL_PATH}/sysroots/x86_64-linux/bin/arm-openwrt-linux-muslgnueabi-ar")
set(CMAKE_LD "${CROSSTOOL_PATH}/sysroots/x86_64-linux/bin/arm-openwrt-linux-muslgnueabi-ld")
set(CMAKE_NM "${CROSSTOOL_PATH}/sysroots/x86_64-linux/bin/arm-openwrt-linux-muslgnueabi-nm")
set(CMAKE_OBJCOPY "${CROSSTOOL_PATH}/sysroots/x86_64-linux/bin/arm-openwrt-linux-muslgnueabi-objcopy")
set(CMAKE_OBJDUMP "${CROSSTOOL_PATH}/sysroots/x86_64-linux/bin/arm-openwrt-linux-muslgnueabi-objdump")
set(CMAKE_RANLIB "${CROSSTOOL_PATH}/sysroots/x86_64-linux/bin/arm-openwrt-linux-muslgnueabi-ranlib")

# Specify the target architecture
set(CMAKE_SYSTEM_NAME Linux)
#set(CMAKE_SYSTEM_NAME UNIX)
set(CMAKE_SYSTEM_PROCESSOR arm)

# Optional: Set any flags or other settings specific to the toolchain
# ...

# Optional: Add path to the toolchain binaries to the system PATH

#link_directories(${CROSSTOOL_PATH}/sysroots/target-arm)
include_directories(${CROSSTOOL_PATH}/sysroots/target-arm/usr/include)
#link_directories(${CROSSTOOL_PATH}/sysroots/target-arm/usr/lib)
link_directories(${CROSSTOOL_PATH}/sysroots/x86_64-linux/lib)
include_directories(${CROSSTOOL_PATH}/sysroots/x86_64-linux/include)
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -L${CROSSTOOL_PATH}/sysroots/x86_64-linux/lib -latomic")
set(STAGING_DIR ${CROSSTOOL_PATH}/sysroots/x86_64-linux/bin)

