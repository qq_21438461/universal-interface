### Title: Public API Project

### Description:

The Public API Project is a comprehensive library designed to streamline and simplify the integration and usage of various essential utilities and services in software development projects. It is modular and provides a set of APIs that cater to error handling, JSON manipulation, logging, random number generation, thread management, and internal module factory creation and management.

#### Features:

- **ErrorHandler:** Enhanced functionality for handling and reporting runtime errors, exceptions, and signals. It is equipped with a Linux-specific signal handler to ensure robust error management across different scenarios.

- **JSONHandler:** A utility for efficient JSON data handling, enabling easy manipulation, querying, and transformation of JSON objects.

- **LogHandler:** A logging utility designed for simplified and configurable logging, ensuring that application logs are captured, categorized, and stored effectively.

- **RandomProvider:** Provides reliable and efficient generation of random numbers, ensuring diversity and unpredictability, supporting various applications and use cases.

- **ThreadModule:** A utility for streamlined thread management, offering functionalities to create, manage, and monitor multiple threads effectively.

- **InternalModuleFactory:** A foundational component for creating and managing internal modules, ensuring that the modular architecture is easy to extend, manage, and maintain.

This project aims to offer a plug-and-play experience, where each module is well-contained, easy to integrate, and highly configurable to adapt to various development needs and scenarios. Each API is crafted with attention to performance, reliability, and usability, making the Public API Project a go-to solution for developers looking to enhance their projects with reliable utilities.

### Installation and Configuration

Installing and configuring the Public API Project is straightforward, thanks to the inclusion of platform-specific build scripts housed within the `scripts` directory. Each script is tailored to ensure the correct compiler and CMake parameters are utilized for various platforms.

#### Installation Steps:

1. **Clone the Repository:**
   First, clone the repository to your local machine using git.

```shell
git clone https://gitcode.net/csdn_codechina/2.14-3.14.git
cd universal-interface
```

2. **Run the Build Script:**
   Navigate to the `scripts` directory and execute the appropriate build script for your platform. For example, for Ubuntu, you would run the `build_ubuntu.sh` script.

```shell
./scripts/build_ubuntu.sh
```

This script initiates the compilation process, specifying the compiler and CMake parameters. All compiled files are neatly organized and stored in the `build` directory upon completion.

3. **Access Compiled Files:**
   Post-compilation, the header files and libraries are automatically copied to the `include` and `lib` directories respectively, within the `Release` folder. You can directly link these in your project.

```shell
cd Release
ls    # You should see the 'include' and 'lib' directories listed
```

#### Testing (Optional):

If you’re interested in running tests to validate the installation, navigate to the `build` directory and execute the test binaries.

#### Using the APIs:

With the header files and libraries readily available in the `include` and `lib` directories, you can easily incorporate them into your development projects. Ensure to link the libraries during the compilation of your project.

#### Note:

The build scripts are tailored for different platforms, ensuring seamless compilation and setup process across various development environments.

### That’s it!

You have successfully installed and configured the Public API Project. You can now explore and utilize the various APIs and utilities available, enhancing your software development projects with optimized, reliable, and efficient solutions.