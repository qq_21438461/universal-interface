#include <iostream>
#include <string>
#include <map>
#include <memory>
#include "config.h"
//#include "JSONHandler/JSONHandler.h"
//#include "Release/include/JSONHandler.h"
#include "Release/include/Module/ErrorHandler/signalhandler_linux.h"
#include "Release/include/InterfaceSDK.h"
#include "Release/include/Module/ThreadModule/ThreadPool.h"
#include "Release/include/Module/RandomProvider/RandomProvider.h"
// A custom signal handler function
void customSignalHandler(int signum, siginfo_t *info, void *context) {
    std::cout << "Caught signal " << signum << std::endl;

    // Get the instance of the Exceptionhandler
    Exceptionhandler& handler = Exceptionhandler::get_instance();

    // Get and print stack trace
    std::vector<std::string> stackTrace;
    handler.getStackTrace(stackTrace);
    std::cout << "Stack Trace:" << std::endl;
    for (const auto& line : stackTrace) {
        std::cout << line << std::endl;
    }
    

    // Get and print system information
    std::string sysInfo;
    if (handler.getSysinfo(sysInfo)) {
        std::cout << "System Info: " << sysInfo << std::endl;
    }

    // Get and print CPU usage information
    std::string cpuUsageInfo;
    if (handler.getProcessCPUUsage(cpuUsageInfo)) {
        std::cout << "CPU Usage Info: " << cpuUsageInfo << std::endl;
    }

    // Get and print resource usage information
    std::vector<std::string> resourceUsage;
    if (handler.getProcessResourceUsage(resourceUsage)) {
        std::cout << "Resource Usage:" << std::endl;
        for (const auto& usage : resourceUsage) {
            std::cout << usage << std::endl;
        }
    }

    // Get and print thread status information
    std::vector<ThreadInfo> threadStatus;
    if (handler.getThreadStatus(threadStatus)) {
        std::cout << "Thread Status:" << std::endl;
        for (const auto& thread : threadStatus) {
            std::cout << "Thread ID: " << thread.tid << ", Name: " << thread.comm << ", State: " << thread.state << std::endl;
        }
    }
}
int main(int argc, char* argv[])
{
   // JSONHandler handler;
   // std::shared_ptr<CC_SDK> sdk = std::make_shared<CC_SDK>();
    // add a little delay to make sure the program is running to the end
    for(int i = 0; i < 5212; i++)
    {
      usleep(100);
   //   std::cout << "argv[" << i << "] = " << i << std::endl;
    }


    ThreadPool pool(5); // Create a ThreadPool with 4 threads
    pool.setLogEnabled(true);
    std::vector<std::future<int>> results;
    for(int i = 0; i < 10; i++) {
       // std::this_thread::sleep_for(std::chrono::milliseconds(1)); 

        int p = RandomProvider::getInstance().getRandomValue<int>({-20, 20});

        //auto result = pool.enqueue([](int answer) { std::this_thread::sleep_for(std::chrono::milliseconds(5));  return answer; }, i);
        auto result = pool.enqueueWithPriority(p,[](int answer) { std::this_thread::sleep_for(std::chrono::milliseconds(10));  return answer; }, i);
        results.push_back(std::move(result));
    }

    for(auto &fut : results) {
        std::cout << fut.get() << std::endl;
    }
    std::cout << "end" << std::endl;
    exit(0);



    std::string info;
    info = Exceptionhandler::get_instance().getAllInfo();
    std::cout << "Exception Info: " << std::endl;
    std::cout << info << std::endl;
    InterfaceSDK sdk;
    std::cout << "start test" <<std::endl;
//#ifdef COMPILER_ARCH_NOT_AARCH64
    //handler.createRNGJsonFile("temp.json",512,5);
//#endif
    sdk.JSONHandler_createConfigJsonFile("config.json");

    std::cout << "--------------------" <<std::endl;
    sdk.JSONHandler_loadFromJsonFile("config.json");
  //  handler.loadFromJsonFile("temp.json");
    if(argc >= 2)
    {
      std::cout << "Find " << argv[1] << std::endl;
      std::cout << "result :" << sdk.JSONHandler_containsKey(argv[1]) << std::endl;
    }

    std::cout << sdk.JSONHandler_getJsonttoStdstring() << std::endl;

    std::cout << "--------------------" <<std::endl;
    std::map<std::string, std::string> map;
    std::string key = "ASHNVZFYTUGAO";
   // handler.getAllValue(&key, &map,false);
#ifdef COMPILER_ARCH_NOT_AARCH64
    //  auto a = handler.getValueVariant<int>("cycleTime");
    //  if(a.has_value()){
    //    std::cout << a.value() << std::endl;
    //  }
#endif
    if(sdk.JSONHandler_containsKey("cycleTime")){
       auto res = sdk.JSONHandler_getValue("cycleTime");
       std::cout << "find " << "res :" <<res <<std::endl;
    }
    
    // Step 1: Get the instance of the Exceptionhandler
    Exceptionhandler& handler = Exceptionhandler::get_instance();

    // Step 2: Register the custom signal handler
    handler.bindingFailureSignalHandler(customSignalHandler);

    // Step 3: Trigger a signal
    // For demonstration, we're raising a SIGTERM signal
    // In a real-world scenario, this would be triggered by an actual fault condition
    raise(SIGTERM);


    return 0;
}