/**
 * @file JSONHandler.h
 * @brief Header file for the JSONHandler class, a comprehensive solution for JSON manipulation.
 *
 * This file introduces the JSONHandler class, crafted to simplify interactions with JSON data, including parsing,
 * querying, manipulating, and saving JSON structures. It builds upon the popular nlohmann::json library to offer a
 * streamlined interface for handling JSON data effectively. The class is designed to be versatile, supporting operations
 * like loading JSON from files or strings, navigating through JSON objects, updating values, and persisting changes back
 * to files.
 *
 * With the optional inclusion of C++17 features, JSONHandler extends its capabilities to include advanced data handling
 * using std::variant, std::optional, and std::filesystem, enhancing its utility in modern C++ applications. Features like
 * thread-safe operations, dynamic JSON object generation, and efficient value retrieval using modern C++ paradigms, make
 * it an essential tool for developers dealing with JSON in C++.
 *
 * The class's design emphasizes safety and efficiency, with operations ensuring thread safety and the class being non-copyable
 * and non-movable to maintain data integrity. JSONHandler serves as a foundational component for applications requiring robust
 * and flexible JSON data manipulation, catering to a wide range of use cases from configuration management to data serialization
 * and beyond.
 *
 * Usage scenarios include but are not limited to, loading configuration files, updating settings dynamically, generating JSON
 * data for testing, and serializing application data for storage or network transmission. The comprehensive feature set and
 * the inclusion of C++17 enhancements make JSONHandler a versatile and powerful tool for modern C++ developers working with JSON data.
 *
 * @author liuzhiyu (liuzhiyu27@foxmail.com)
 * @copyright Copyright (c) 2023 by liuzhiyu, All Rights Reserved.
 */

#pragma once
#include <string>
#include <vector>
#include <map>
#include <memory>
#include <mutex>
#include <functional>
#include <iostream>
#include <fstream>
#include <atomic>
#include "DefaultInternalModuleFactory.hpp"
#include "nlohmann/json.hpp"
#include "SoftWareVersion.h"

#ifdef USE_CPP17_FEATURES 
    #include <variant>
    #include <optional>
    #include <filesystem>
    #include "RandomProvider/RandomProvider.h"
    #include "JSONValueHandler.hpp"
#endif

template <typename T>
struct map_traits;
// Helper structs to check for the existence of emplace and push_back
template <template <typename, typename, typename...> class Map, typename Key, typename Value, typename... Rest>
struct map_traits<Map<Key, Value, Rest...>> {
    using key_type = Key;
    using mapped_type = Value;
};

/**
 * @class JSONHandler
 * @brief A comprehensive class for handling various JSON operations.
 *
 * JSONHandler is designed to facilitate a wide range of operations on JSON data, 
 * making it easy to parse, query, and manipulate JSON structures. It supports loading 
 * JSON data from files or strings, exploring and modifying JSON objects, and saving 
 * JSON data back to files in a structured manner.
 *
 * The class leverages the nlohmann::json library for JSON parsing and manipulation, 
 * providing a user-friendly interface for common JSON operations. This includes finding 
 * specific keys, extracting values of various types, modifying JSON structures, and 
 * serializing JSON objects to strings or files.
 *
 * With optional C++17 features enabled, JSONHandler offers advanced functionalities like 
 * storing JSON values in std::variant containers and generating random JSON objects. This 
 * makes the class suitable for applications that require dynamic and flexible handling of 
 * JSON data, such as configuration management, data serialization/deserialization, and more.
 *
 * Key features:
 * - Load and parse JSON from files or strings.
 * - Query and manipulate JSON data easily.
 * - Save JSON objects to files with customizable formatting.
 * - Thread-safe operations on JSON objects.
 * - Advanced C++17 features for enhanced JSON value handling.
 *
 * Example Usage:
 *     JSONHandler jsonHandler;
 *     jsonHandler.loadFromJsonFile("config.json");
 *     auto value = jsonHandler.getValue("settings/key");
 *     jsonHandler.setJsonValue("settings/newKey", "newValue");
 *     jsonHandler.saveToFile("updated_config.json");
 *
 * @note This class is not copyable or movable to ensure safe handling of resources 
 *       and maintain the integrity of JSON data.
 */
class __attribute__((visibility("default"))) JSONHandler
{
public:
    /**
     * @brief Constructor that builds a JSON object from a file.
     * @param path Path to the JSON file.
     */
    JSONHandler(const std::string& path);

    /**
     * @brief Default constructor.
     */
    JSONHandler();

    /**
     * @brief Default destructor.
     */
    ~JSONHandler() = default;

    // Delete copy and move constructors and assignment operators
    JSONHandler(const JSONHandler&) = delete;
    JSONHandler& operator=(const JSONHandler&) = delete;
    JSONHandler(JSONHandler&&) = delete;
    JSONHandler& operator=(JSONHandler&&) = delete;
  /**
   * @brief Initializes the JSONHandler with a given source string.
   * 
   * The source string can either be a file path or a JSON string.
   * - If it is a valid file path, the function will attempt to load JSON from that file.
   * - If it is not a valid file path, the function will attempt to parse it as a JSON object.
   * 
   * @param source Pointer to the source string, which can be a file path or a JSON string.
   * @return True if JSON is successfully loaded or parsed, false otherwise.
   */
  bool init(const std::string* source);

  /**
   * @brief Resets the JSON object from a file or a JSON string.
   * 
   * @param source Pointer to the source string for resetting.
   * @return True on successful reset, false otherwise.
   */
  bool reset(const std::string* source) noexcept;

  /**
   * @brief Clears the current JSON object.
   */
  void clear() noexcept;

  /**
   * @brief Loads a JSON object from a file.
   * 
   * @param path Path to the file from which to load the JSON.
   * @return True if loading is successful, false otherwise.
   */
  bool loadFromJsonFile(const std::string& path);
/**
   * @brief Checks if the JSON object contains a specific key.
   * 
   * @param key The key to check for in the JSON object.
   * @return True if the key exists, false otherwise.
   */
  bool containsKey(const std::string& key);

  /**
   * @brief Gets the value of the main key in the JSON object.
   * 
   * @return The string value of the main key.
   */
  inline std::string getMainKey() const noexcept {
      return m_mainKey;
  }
  /**
   * @brief Converts the internal JSON object to a string format.
   * 
   * @return A string representation of the JSON object.
   */
  std::string getJsonttoStdstring(){   
    return m_jsonObject.dump();
  }
    /**
     * @brief Traverses a nlohmann::json object and retrieves all key-value pairs based on specified conditions.
     *
     * If a key is provided, the function only traverses the children of that key.
     * Otherwise, it traverses the entire JSON object.
     *
     * @param key Pointer to a string specifying the starting key for traversal.
     *            If nullptr, the function traverses the entire JSON object.
     * @param resultMap Pointer to a map for storing results.
     *                  If key is not nullptr, resultMap should also not be nullptr.
     *                  If both key and resultMap are nullptr, the function uses the internal m_jsonStringMap for storage.
     * @param topLevelOnly If true, only top-level objects are traversed without recursion into nested objects or arrays.
     *
     * @return True if at least one key-value pair is found, false otherwise.
     */
  bool getAllValue(const std::string* key = nullptr, std::map<std::string, std::string>* resultMap = nullptr, bool topLevelOnly = false) noexcept;
    /**
     * @brief Overloaded function of getAllValue for direct string key input.
     *
     * @param key A string specifying the starting key for traversal.
     * @param resultMap Reference to a map for storing results.
     * @param topLevelOnly If true, only top-level objects are traversed without recursion into nested objects or arrays.
     *
     * @return True if at least one key-value pair is found, false otherwise.
     */  
  bool getAllValue(const std::string& key, std::map<std::string, std::string>& resultMap, bool topLevelOnly = false) noexcept {
      return getAllValue(&key, &resultMap, topLevelOnly);
  }
    /**
     * @brief Retrieves the value associated with a specified key in the JSON object.
     *
     * @param key The key for which the value is to be retrieved.
     * @return The value as a string, associated with the specified key.
     */
  std::string getValue(const std::string& key);
#ifdef USE_CPP17_FEATURES
/**
 * @brief Traverses a nlohmann::json object and stores the found values in a std::variant container.
 *
 * If a key is provided, traversal starts from that key.
 * Otherwise, traversal begins from the top level of the entire JSON object.
 *
 * @param key Pointer to a string specifying the starting key for traversal.
 *            If nullptr, traversal starts from the top level of the JSON object.
 * @param topLevelOnly If true, only top-level objects are traversed without recursion into nested objects or arrays.
 *
 * @return True if at least one key-value pair is found and successfully stored in std::variant. False otherwise.
 */
bool getAllValueVariant(const std::string* key = nullptr, bool topLevelOnly = false){
    auto targetkey =  key ? *key : m_mainKey;
    exploreJsonForKey(m_jsonObject, [&](const std::string& k, const nlohmann::json& v) {
        if (!v.is_object() && !v.is_array()) {
            if (v.is_string()) {
                storeInVariant<std::string>(k, v);
            } else if (v.is_number_float()) {
                storeInVariant<float>(k, v);
            } else if (v.is_number_integer()) {
                storeInVariant<int>(k, v);
            } else if (v.is_boolean()) {
                storeInVariant<bool>(k, v);
            } else if (v.is_null()) {
                storeInVariant<std::nullptr_t>(k, v);
            }
        }
    }, topLevelOnly, &targetkey);
    std::cout << "m_jsonValueMap size: " << m_jsonValueMap.size() << std::endl;
    return !m_jsonValueMap.empty();
}
/**
 * @brief Retrieves a value of a specific type associated with a given key, stored in a std::variant.
 *
 * @tparam T The expected type of the value to retrieve.
 * @param key The key for which the value is to be retrieved.
 * @return An std::optional containing the value if found and of the expected type, or an empty std::optional if not found.
 */
template <typename T>
std::optional<T> getValueVariant(const std::string& key){
     if(m_jsonValueMap.empty()){
      getAllValueVariant();
    }
    auto it = m_jsonValueMap.find(key);
    if (it != m_jsonValueMap.end()) {
        if (std::holds_alternative<T>(it->second)) {
            return std::get<T>(it->second);
        }
    }
    return std::nullopt;  // 返回空的 std::optional，表示没有找到
}
#endif
  /**
   * @brief Saves the JSON object to a file with optional formatting.
   * 
   * @param outputFileName The name of the file where the JSON object should be saved.
   * @param dumpValue Formatting option for the JSON output (e.g., indentation level).
   * @param outputjson The JSON object to be saved.
   * @return True if the file was successfully written, false otherwise.
   */
  bool saveToFile(const std::string& outputFileName, uint8_t dumpValue, const nlohmann::json &outputjson);
  /**
   * @brief Overload of saveToFile, using the internal JSON object.
   * 
   * @param outputFileName The name of the file where the JSON object should be saved.
   * @param dumpValue Formatting option for the JSON output (default is 4).
   * @return True if the file was successfully written, false otherwise.
   */
  bool saveToFile(const std::string& outputFileName, uint8_t dumpValue = 4){
    return saveToFile(outputFileName, dumpValue, m_jsonObject);
  }
/**************************  create Json **************************************/  
#ifdef USE_CPP17_FEATURES 

  void createRNGJsonFile(const std::string& path,std::size_t Max_Size_byte = 1024*1024*1,int MaxDepth = 10);  
#endif
  /**
   * @brief Creates and saves a default configuration file or one based on a provided map.
   * 
   * @param path The path where the configuration file should be saved.
   * @param configMap Pointer to a map containing key-value pairs for the configuration. 
   *                  If nullptr, default configuration items are used.
   * @return True if the configuration file is successfully saved, false otherwise.
   */
  bool createConfigJsonFile(const std::string& path,std::map<std::string, std::string> *configMap = nullptr);  
/**************************  set Json **************************************/  
  /**
   * @brief Sets the current key for the JSON object.
   * 
   * @param key The key to be set as the current key.
   */
  void setCurrentKey(const std::string& key) noexcept{
    std::lock_guard<std::mutex> lock(m_keymutex);
    m_currentKey = key;
  }
  /**
   * @brief Sets a value in the JSON object for a given key.
   * 
   * @tparam T The type of the value being set.
   * @param key The key for which the value is to be set.
   * @param value The value to be set in the JSON object.
   */
  template <typename T>
  void setJsonValue(const std::string& key, T& value)
  {
    std::lock_guard<std::mutex> lock(m_keymutex);
    m_jsonObject[key] = value;
    m_isDirty = true;
  }
private:
  /**
   * @brief Retrieves the JSON object, optionally filtered by a given key.
   * 
   * If a key is provided, this function returns the JSON object associated with that key. 
   * If no key is provided, it returns the entire JSON object.
   * 
   * @param key Optional pointer to a string representing the key. If nullptr, the entire JSON object is returned.
   * @return A reference to the nlohmann::json object, either the whole object or the part associated with the given key.
   */
  nlohmann::json& getJsonObject(std::string* key = nullptr);

  /**
   * @brief Sets the main key for the JSON object.
   * 
   * This function sets the specified key as the main key of the JSON object. 
   * It is thread-safe as it uses a mutex to protect the assignment operation.
   * 
   * @param key The string representing the key to be set as the main key.
   */
  void setMainKey(const std::string& key) noexcept {
      std::lock_guard<std::mutex> lock(m_keymutex);
      m_mainKey = key;
  }
  /**
   * @brief Finds a specified key in a nlohmann::json object.
   * 
   * The function traverses the JSON object using exploreJsonForKey. Once the matching key is found,
   * it stops the traversal immediately by throwing an exception.
   * 
   * @param key The key to find in the JSON object.
   * @param json_obj The nlohmann::json object to traverse.
   * @param topLevelOnly If true, only the top-level objects are traversed. Nested objects or arrays are not traversed.
   * @return True if the specified key is found in the JSON object, false otherwise.
   * 
   * @note This function uses exceptions to exit the traversal early. These exceptions are internally caught and handled, so they do not affect the caller.
   */
bool findKeyHelper(const std::string& key, const nlohmann::json& json_obj, bool topLevelOnly = false){
    bool found = false;
    try {
        exploreJsonForKey(json_obj, [&](const std::string& k, const nlohmann::json& v) {
            if (k == key) {
                found = true;
                throw true;  // 使用异常提前退出遍历
            }
        }, topLevelOnly);
    } catch (bool) {
        std::cout << "findKeyHelper return true" << std::endl; 
    }
    return found;
  }
  /**
   * @brief Recursively traverses a nlohmann::json object, executing a callback function for each key-value pair.
   * 
   * @tparam Callback Type of the callback function. It should be a callable object accepting two parameters:
   *                  1) Key (type const std::string&)
   *                  2) Value (type const nlohmann::json&)
   * @param json_obj The nlohmann::json object to traverse.
   * @param cb The callback function to be executed for each key-value pair.
   * @param topLevelOnly If true, only the top-level objects are traversed. Nested objects or arrays are not traversed.
   * @param startKey A pointer to a string specifying the key from which to start traversing.
   */

template <typename Callback>
void exploreJsonForKey(const nlohmann::json& json_obj, Callback cb, bool topLevelOnly = false, const std::string* startKey = nullptr){
    if (startKey) {
        if (json_obj.is_object() && json_obj.contains(*startKey)) {
            // 如果当前对象包含 startKey，只遍历这个键下的子对象
            exploreJsonForKey(json_obj[*startKey], cb, topLevelOnly);
        } else if (json_obj.is_object()) {
            // 如果当前对象不包含 startKey，递归遍历每个子对象
            for (const auto& item : json_obj.items()) {
                exploreJsonForKey(item.value(), cb, topLevelOnly, startKey);
            }
        }
    }
    else{
      if (json_obj.is_object()) {
          for (const auto& item : json_obj.items()) {
              // 调用回调函数
              cb(item.key(), item.value());

              // 如果允许，递归遍历子对象
              if (!topLevelOnly) {
                  exploreJsonForKey(item.value(), cb, topLevelOnly);
              }
          }
      }
      // 如果是一个 JSON 数组并且允许遍历子元素，则遍历它
      else if (json_obj.is_array() && !topLevelOnly) {
          for (const auto& element : json_obj) {
              exploreJsonForKey(element, cb, topLevelOnly);
          }
      }
    }
  }
#ifdef USE_CPP17_FEATURES 
  /**
   * @brief Generates a random object within the given parent JSON object.
   * 
   * @tparam T Type of the optional pair to be returned, defaults to std::pair<std::string, std::string>.
   * @param parentObject The parent JSON object within which the random object is to be set.
   * @param outputPair An optional output pair that, if provided, will be filled with the key and value of the generated object.
   * @return True if a random object is successfully created, false if the parent object is not a JSON object.
   */
  template <typename T = std::pair<std::string, std::string>>
  inline bool setRandomObject(nlohmann::json& parentObject,std::optional<T>* outputPair = nullptr) const {
    if (!parentObject.is_object()) {
        return false;
    }
    std::string currentKey;
    do {
        currentKey = m_randomProvider->getRandomValue<std::string>({5, 15});
    } while (parentObject.contains(currentKey));

    int value_type = m_randomProvider->getRandomValue<int>({1, 4});
    switch (value_type) {
        case 1:
            parentObject[currentKey] = m_randomProvider->getRandomValue<int>({0, 100});
            break;
        case 2:
            parentObject[currentKey] = m_randomProvider->getRandomValue<float>({0.00, 20.00});
            break;
        case 3:
            parentObject[currentKey] = m_randomProvider->getRandomValue<std::string>({10, 20});
            break;
        case 4:
            parentObject[currentKey] = m_randomProvider->getRandomValue<bool>();
            break;
    }

   if (outputPair) {
        if constexpr (std::is_same_v<T, std::pair<std::string, std::string>>) {
            *outputPair = std::optional<T>(std::make_pair(currentKey, parentObject[currentKey].dump()));
        } else if constexpr (std::is_same_v<T, std::pair<std::string, nlohmann::json>>) {
            *outputPair = std::optional<T>(std::make_pair(currentKey, parentObject[currentKey]));
        }
    }
    return true;
  }
  /**
   * @brief Retrieves a random JSON object from the given parent object.
   * 
   * @param parentObject The parent JSON object from which to retrieve a random object.
   * @return An optional reference wrapper to a JSON object, if found.
   */
  std::optional<std::reference_wrapper<nlohmann::json>> getRandomObject(nlohmann::json& parentObject) const;

  /**
   * @brief Retrieves a JSON object at a specific depth level.
   * 
   * @param rootKey A pointer to the root JSON object.
   * @param targetDepth The depth level at which to retrieve the JSON object.
   * @return An optional reference wrapper to a JSON object at the specified depth, if found.
   */
  std::optional<std::reference_wrapper<nlohmann::json>> getTargetLevel(nlohmann::json* rootKey, int targetDepth) const;
#endif
private:
/**
 * @brief The main JSON object used for various operations.
 */
nlohmann::json m_jsonObject;

/**
 * @brief The path of the current file being processed.
 */
std::string m_currentfile;

/**
 * @brief The main key of the JSON object, used as a reference point for certain operations.
 */
std::string m_mainKey;

/**
 * @brief The current key of the JSON object, used for tracking the current point of focus within the JSON structure.
 */
std::string m_currentKey;

/**
 * @brief Mutex for thread-safe operations on keys.
 */
std::mutex m_keymutex;

/**
 * @brief Map for storing string representations of JSON key-value pairs.
 */
std::map<std::string, std::string> m_jsonStringMap;

/**
 * @brief Map for storing string representations of the current JSON key-value pairs.
 */
std::map<std::string, std::string> m_currentJsonStringMap;

/**
 * @brief Atomic boolean flag indicating whether the JSON object has been modified.
 */
std::atomic_bool m_isDirty;
#ifdef USE_CPP17_FEATURES
    /**
     * @brief Stores a value from a nlohmann::json object into a std::variant.
     *
     * @tparam T The type of the value to be stored.
     * @param k The key associated with the value.
     * @param v The nlohmann::json object from which the value is extracted.
     */
    template<typename T>
    void storeInVariant(const std::string& k, const nlohmann::json& v) {
        JSONValueHandler<T> handler;
        m_jsonValueMap[k] = handler.getValue(v);
    }
/**
 * @brief A unique pointer to a RandomProvider, used for generating random values.
 */
std::unique_ptr<RandomProvider> m_randomProvider;

/**
 * @brief A map for storing various types of values extracted from a JSON object, using std::variant.
 */
std::map<std::string, std::variant<std::string, float, int, bool, std::nullptr_t>> m_jsonValueMap;

/**
 * @brief A map for storing various types of values for the current JSON operation, using std::variant.
 */
std::map<std::string, std::variant<std::string, float, int, bool, std::nullptr_t>> m_currentJsonValueMap;
#endif
};  
